public class Circle {
    double radius = 1.0;

    public Circle(){
        System.out.println("Ko có tham số!");
    };

    public Circle(double radius) {
        this.radius = radius;
    }

    public double getRadius() {
        return radius;
    }

    public void setRadius() {
        this.radius = radius;
    }

    public double getArea(double radius) {
        return radius * radius * 3.14;
    }

    public double getCircumference(double radius) {
        return radius * 2 * 3.14;
    }

    @Override
    public String toString() {
        return "Dien tich hinh tron: " + getArea(radius) + ", Chu vi hinh tron: " + getCircumference(radius);  
    }
}
