public class App {
    public static void main(String[] args) throws Exception {
        System.out.println("Hello, World!");

        Circle circle1 = new Circle();
        Circle circle2 = new Circle(3.0);

        System.out.println(circle1 + ", " + circle2);
    }
}
